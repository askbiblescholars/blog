# utils.py

from django.core.mail import send_mail
from django.conf import settings

def sendEmail(user, profile, password):
    email_subject = 'Welcome to Our Website'
    link = "https://localhost:8000/account/reset-password/{}".format(user.email) if settings.DEBUG else "https://localhost:8000/account/reset-password/{}".format(user.email)
    email_content = f'<div><p>Hello {user.username},  welcome to our website!</p> </br> <p>Password: {password}</p> </br> <p>Role: {profile.role}</p> </br> <a href="{link}">Reset Password (Link)</a>  </div>'
    send_mail(
        subject=email_subject,
        message='',
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        html_message=email_content
    )