from django.urls import path
# from . import views
from .views import *


urlpatterns = [
    # path('', views.home, name="home"),
    path('', HomeView.as_view(), name="home"),
    path('article/<int:pk>', ArticleDetailView.as_view(), name='article-detail'),
    path('add_post/', AddPostView.as_view(), name='add_post'),
    path('pending-posts/', pendingPosts, name='pending-posts'),
    path('my-posts/', myPosts, name='my-posts'),
    path('add-reason/', AddReason, name='add-reason'),
    path('activate-post/<int:pk>', activatePost, name='activate-post'),
    path('manage-users/', ManageUsers, name='manage-users'),
    path('edit-user/<int:pk>', EditUserAndProfileView.as_view(), name='edit-user'),
    path('account/reset-password/<str:email>',
         resetPassword, name='reset-password'),
    path('remove-user/<int:pk>', RemoveUser, name='remove-user'),
    path('add-user/', AddUserView.as_view(), name='add-user'),
    path('add-tag/', AddTags, name='add-tag'),
    path('delete-tag', DeleteTag, name='delete-tag'),
    path('add_category/', AddCategoryView.as_view(), name='add_category'),
    path('article/edit/<int:pk>', UpdatePostView.as_view(), name='update_post'),
    path('article/<int:pk>/remove', DeletePostView.as_view(), name='delete_post'),
    path('category/<str:cats>/', CategoryView, name='category'),
    path('category-list/', CategoryListView, name='category-list'),
    path('like/<int:pk>', LikeView, name='like_post'),
    path('article/<int:pk>/comment/',
         AddCommentView.as_view(), name='add_comment'),
    path('edit-comment/<int:pk>/', editComment, name='edit-comment'),
    path('hide-comment/<int:pk>/', hideComment, name='hide-comment'),
    path('update-comment/<int:pk>/', updateComment, name='update-comment'),

]
