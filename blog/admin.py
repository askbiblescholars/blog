from django.contrib import admin
from .models import Post, Category, Profile, Comment, RejectionReason

# Make blog posts accessible from the administration area
admin.site.register(Post)
admin.site.register(Category)
admin.site.register(Profile)
admin.site.register(Comment)
admin.site.register(RejectionReason)
