from django.contrib.auth.hashers import make_password
from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect, HttpResponse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Post, Category, Comment, Profile, Tag, RejectionReason
from .forms import PostForm, EditForm, CommentForm, UserForm, ProfileForm, ResetPasswordForm, EditUserForm, EditProfileForm
from django.urls import reverse_lazy, reverse, resolve
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.views import View
from .utils import sendEmail
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

# def home(request):
#    return render(request, 'home.html', {})


def LikeView(request, pk):
    post = get_object_or_404(Post, id=request.POST.get('post_id'))
    liked = False
    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
        liked = False
    else:
        post.likes.add(request.user)
        liked = True

    return HttpResponseRedirect(reverse('article-detail', args=[str(pk)]))


class HomeView(ListView):
    model = Post
    template_name = 'home.html'
    ordering = ['-post_date']
    # order posts by primary key in reverse order
    # ordering = ['-id']

    def get_queryset(self):
        return super().get_queryset().filter(approved=True)

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        context["cat_menu"] = cat_menu
        return context

# function method in lieu of class method


def CategoryListView(request):
    cat_menu_list = Category.objects.all()
    return render(request, 'category_list.html', {'cat_menu_list': cat_menu_list})


def CategoryView(request, cats):
    category_posts = Post.objects.filter(
        category__icontains=cats.replace('-', ' '), approved=True)
    return render(request, 'categories.html', {'cats': cats.title().replace('-', ' '), 'category_posts': category_posts})


class ArticleDetailView(DetailView):
    model = Post
    template_name = 'article_details.html'

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(ArticleDetailView, self).get_context_data(
            *args, **kwargs)

        stuff = get_object_or_404(Post, id=self.kwargs['pk'])
        total_likes = stuff.total_likes()

        liked = False
        if stuff.likes.filter(id=self.request.user.id).exists():
            liked = True

        context["cat_menu"] = cat_menu
        context['total_likes'] = total_likes
        context['liked'] = liked
        return context

class AddPostView(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'add_post.html'
    # fields = '__all__'
    # fields = ('title', 'body')

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(AddPostView, self).get_context_data(*args, **kwargs)
        context["cat_menu"] = cat_menu
        return context

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        user = self.request.user
        if not hasattr(user, "profile") or user.profile.role == 'end_user':
            form.fields.pop('snippet')
            form.fields.pop('category')
            form.fields.pop('title_tag')
        return form


def pendingPosts(request):
    posts = Post.objects.filter(approved=False)
    return render(request, "pending-posts.html", {"posts": posts})


def myPosts(request):
    posts = Post.objects.filter(author_id=request.user.id)
    return render(request, "my-posts.html", {"posts": posts})


def activatePost(request, pk):
    post = Post.objects.get(id=pk)
    if post:
        post.approved = True
        post.save()
        return redirect("pending-posts")
    return redirect("pending-posts")

def AddReason(request):
    post_id = request.POST.get("post_id")
    reason = request.POST.get("reason")
    reason_obj = RejectionReason.objects.create(post_id=post_id, reason=reason)
    reason_obj.save()
    messages.success(request, 'Reason Added Successfully!')
    return redirect("pending-posts")
    


class AddCommentView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'add_comment.html'
    success_url = reverse_lazy('home')
    # fields = '__all__'

    # Figure out which user is using this form and then continue processing.
    # Look back at the page that is using Javascript and replace with this method.

    def form_valid(self, form):
        form.instance.post_id = self.kwargs['pk']
        form.instance.user = self.request.user
        return super().form_valid(form)


def editComment(request, pk):
    comment = Comment.objects.get(id=pk)
    if comment:
        return render(request, "edit-comment.html", {"comment": comment})
    messages.error(request, 'No Comment Found!')
    return redirect(request.META.get('HTTP_REFERER'))

def updateComment(request, pk):
    name = request.POST.get("name")
    body = request.POST.get("body")
    comment = Comment.objects.get(id=pk)
    if comment:
        comment.name = name
        comment.body = body
        comment.save()
        return redirect("article-detail/"+comment.post_id)
        
    messages.error(request, 'No Comment Found!')
    return redirect(request.META.get('HTTP_REFERER'))

def hideComment(request, pk):
    comment = Comment.objects.get(id=pk)
    comment.hidden = True
    comment.save()
    if comment:
        return redirect(request.META.get('HTTP_REFERER'))
    messages.error(request, 'No Comment Found!')
   

class AddCategoryView(CreateView):
    model = Category
    # form_class = PostForm
    template_name = 'add_category.html'
    fields = '__all__'
    # fields = ('title', 'body')

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(AddCategoryView, self).get_context_data(
            *args, **kwargs)
        context["cat_menu"] = cat_menu
        return context


class UpdatePostView(UpdateView):
    model = Post
    form_class = EditForm
    template_name = 'update_post.html'
    # fields = ['title', 'title_tag', 'body']

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(UpdatePostView, self).get_context_data(*args, **kwargs)
        context["cat_menu"] = cat_menu
        return context


class DeletePostView(DeleteView):
    model = Post
    template_name = 'delete_post.html'
    success_url = reverse_lazy('home')

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(DeletePostView, self).get_context_data(*args, **kwargs)
        context["cat_menu"] = cat_menu
        return context


def ManageUsers(request):
    users = User.objects.filter(
        is_active=True, is_superuser=False).select_related('profile').all()
    return render(request, "manage_users.html", {"users": users})


def RemoveUser(request, pk):
    user = User.objects.get(id=pk)
    user.is_active = False
    user.save()
    return redirect("/manage-users")


def resetPassword(request, email):
    if request.method == 'POST':
        form = ResetPasswordForm(request.POST)
        email = request.POST.get("email")
        if form.is_valid():
            password = form.cleaned_data['password']

            # Set the new password for the user
            # Code for setting the password goes here
            user = User.objects.filter(email=email).first()
            if user:
                user.password = make_password(password)
                user.save()
            messages.success(request, 'Password has been reset successfully.')
            return redirect('login')
        else:
            messages.error(request, 'Passwords do not match!')
            form = ResetPasswordForm(initial={'email': email})
    else:
        messages.error(request, 'Passwords are required!')
        form = ResetPasswordForm(initial={'email': email})

    return render(request, 'reset-pass.html', {'form': form, 'email': email})


class AddUserView(View):
    def get(self, request):
        user_form = UserForm()
        profile_form = ProfileForm()
        return render(request, 'add_user.html', {'user_form': user_form, 'profile_form': profile_form})

    def post(self, request):
        user_form = UserForm(request.POST)
        profile_form = ProfileForm(request.POST, request.FILES)
        role = request.POST.get("role")

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.is_staff = True
            user.password = make_password(user_form.cleaned_data['password'])
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()

            # Send email to the newly added user
            sendEmail(user, profile, user_form.cleaned_data['password'])
            return redirect('manage-users')

        return render(request, 'add_user.html', {'user_form': user_form, 'profile_form': profile_form})


class EditUserAndProfileView(View):
    def get(self, request, pk):
        user = get_object_or_404(User, pk=pk)
        profileExists = Profile.objects.filter(user_id=user.pk).first()
        if not profileExists:
            profile = Profile.objects.create(user=user, role="end_user").save()
        else:
            profile = get_object_or_404(Profile, user=user)
            
        user_form = EditUserForm(instance=user)
        profile_form = EditProfileForm(instance=profile)
        return render(request, 'edit_user.html', {'user_form': user_form, 'profile_form': profile_form})

    def post(self, request, pk):
        user = get_object_or_404(User, pk=pk)
        profile = get_object_or_404(Profile, user=user)
        user_form = EditUserForm(request.POST, instance=user)
        profile_form = EditProfileForm(request.POST, instance=profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return redirect('manage-users')

        return render(request, 'edit_user.html', {'user_form': user_form, 'profile_form': profile_form})


def AddTags(request):
    if request.method == 'POST':
        tag = request.POST.get("tag")
        profileId = request.POST.get("profileId")
        userId = request.POST.get("userId")

        print(request.body, "*********")
        profileExists = Profile.objects.filter(user_id=userId).first()
        if not profileExists:
            profile = Profile.objects.create(user_id=userId, role="scholar")
            profile.save()

            print(profile)
            profileId = profile.pk

        if tag and profileId:
            tag = Tag.objects.create(name=tag, profile_id=profileId)
            tag.save()

            return redirect("manage-users")

    return redirect("home")


@csrf_exempt
def DeleteTag(request):
    data = json.loads(request.body)
    print(data, data.get('id'))
    tag = Tag.objects.get(id=data.get('id'))
    tag.delete()
    return JsonResponse({"status": "deleted"})
