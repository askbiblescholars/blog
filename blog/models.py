from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime, date
from ckeditor.fields import RichTextField
from enum import Enum


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        # return reverse('article-detail', args=(str(self.id)) )
        return reverse('home')


class RoleChoices(Enum):
    SUPERUSER = 'superuser'
    MODERATOR = 'moderator'
    SCHOLAR = 'scholar'
    QUALITY_CONTRIBUTOR = 'quality_contributor'
    END_USER = 'end_user'


class Profile(models.Model):
    user = models.OneToOneField(
        User, null=True, on_delete=models.CASCADE, related_name="profile")
    bio = models.TextField()
    profile_pic = models.ImageField(
        null=True, blank=True, upload_to="images/profile/")
    website_url = models.CharField(max_length=255, null=True, blank=True)
    facebook_url = models.CharField(max_length=255, null=True, blank=True)
    twitter_url = models.CharField(max_length=255, null=True, blank=True)
    instagram_url = models.CharField(max_length=255, null=True, blank=True)
    pinterest_url = models.CharField(max_length=255, null=True, blank=True)

    class Role(models.TextChoices):
        SUPERUSER = 'superuser', 'Super User'
        MODERATOR = 'moderator', 'Moderator'
        SCHOLAR = 'scholar', 'Scholar'
        QUALITY_CONTRIBUTOR = 'quality_contributor', 'Quality Contributor'
        END_USER = 'end_user', 'End User'

    role = models.CharField(
        max_length=20,
        choices=Role.choices,
        default=Role.END_USER,
    )

    def __str__(self):
        return str(self.user)

    def get_absolute_url(self):
        return reverse('home')


class Tag(models.Model):
    name = models.CharField(max_length=255)
    profile = models.ForeignKey(
        Profile, null=True, on_delete=models.CASCADE, related_name="tags")

    def __str__(self):
        return self.name



class Post(models.Model):
    title = models.CharField(max_length=255)
    title_tag = models.CharField(max_length=255)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    body = RichTextField(blank=True, null=True)
    # body = models.TextField()
    post_date = models.DateField(auto_now_add=True)
    category = models.CharField(max_length=255, default='mainstream')
    snippet = models.CharField(max_length=255)
    likes = models.ManyToManyField(User, related_name='blog_posts')
    approved = models.BooleanField(default=False)

    def total_likes(self):
        return self.likes.count()

    def __str__(self):
        return self.title + ' | ' + str(self.author)

    def get_absolute_url(self):
        # return reverse('article-detail', args=(str(self.id)) )
        return reverse('home')

class RejectionReason(models.Model):
    post = models.ForeignKey(Post, related_name="reject_reason", on_delete=models.CASCADE)
    reason = models.TextField(max_length=255, null=True)
    date_added = models.DateTimeField(auto_now_add=True)


class Comment(models.Model):
    post = models.ForeignKey(
        Post, related_name="comments", on_delete=models.CASCADE)
    user = models.ForeignKey(
        User, related_name="commenter", on_delete=models.CASCADE, null=True, blank=True, default=1)
    name = models.CharField(max_length=255)
    body = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    hidden = models.BooleanField(default=False)

    def __str__(self):
        return '%s - %s' % (self.post.title, self.name)
