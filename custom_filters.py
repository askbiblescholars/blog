from django import template

register = template.Library()

def filter_hidden_comments(comments):
    return comments.filter(hidden=False)

register.filter('filter_hidden_comments', filter_hidden_comments) 